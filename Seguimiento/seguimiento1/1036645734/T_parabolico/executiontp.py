
from tparabolico import TiroParabolico
from tparabolico import TiroParabolicoFcc
import numpy as np
import matplotlib.pyplot as plt

"""
NOTA

4.2

- Se debe comentar mejor las variables
- No funciona con g=0
-Los método de plot se colocan en la clase

"""
if __name__=="__main__":
    #instanciar clase (hacer un objeto)
    xi=0
    h=10
    v0=20
    ang=45
    g=9.8
    afr=8
    t_vuelo=2*v0*np.sin(np.pi/180*ang)/g
    t = np.arange(0,t_vuelo,0.01)
    parabolica=TiroParabolico(xi,h,v0,ang,t)
    parabolicafrcc=TiroParabolicoFcc(xi,h,v0,ang,t,afr)
    #parabolicaAlan=TiroParabolico(5,0,3,45,t)
    
    #parabolica.graficafn(t)

    x_p=parabolica.viewposx()
    y_p=parabolica.viewposy()
    plt.plot(x_p,y_p, label='parábola sin fricción')
    #plt.show()
    #plt.savefig("tiro_aprabolico1.png")

    x_pfr=parabolicafrcc.vfrx()
    y_p=parabolica.viewposy()
    plt.plot(x_pfr,y_p, label='parábola con fricción')
    plt.xlabel("X(m)")
    plt.ylabel("Y(m)")
    plt.grid()
    plt.legend()
    plt.title("Tiro Parabólico")
    plt.show()
    plt.savefig("tiro_aprabolico.png")

    #instanciar clase (friccion)
    #parabolicafcc=TiroParabolicoFcc(0,15,30,2,5)
    #parabolicafcc.graficafnfricc(t)

    """
    
if __name=="__main__";

    velinit=110
    alpha=90
    g=9.0
    h0=0
    x0=0

    #llamar clase
    tirop=TirParabolico(velinit,alpha,g,h0,x0)

    tirop.figMp()
    
    #llamar metodos

    #velX=tirop.velX()
    #velY=tirop.velY()
    #tmax=tirop.tMaxVuelo()
    #arrtime=tirop.arrtime()
    
    print("velocidad en x:{}., velocidad en y:{}.format(velx,vely))
    print("tiempo de vuelo:{}".format(tmax))
    print("txxxxx:{}".format(arrtime))
    

      def graficafn(self,lisT):
        X=[]
        Y=[]
        for t in lisT:
            X.append(self.viewposx(t))
            Y.append(self.viewposy(t))
        plt.plot(X,Y)
        plt.show()
        
 


    """
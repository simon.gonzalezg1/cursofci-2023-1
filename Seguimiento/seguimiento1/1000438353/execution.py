from Particula_en_B import ParticleInB


"""
NOTA

4.2

- NO tiene la informacion de requirements.txt
- revisar los limites, Ex B0=0
- Comentar el código

"""

if __name__=="__main__":

    q=-1
    B0=0#600e-6
    x0=0
    y0=0
    z0=0
    K0=18.6
    m=0.5
    theta=30                
    phi=0

    #Llamar clase
    partB=ParticleInB(q,B0,x0,y0,z0,K0,m,theta,phi)

    #Llamar métodos
    v0=partB.Vel0()
    vx=partB.velX()
    vy=partB.velY()
    vz=partB.velZ()
    


    print("\nMódulo de la velocidad inicial: {}".format(v0))
    print("V0x {}   V0y {}    V0z {} ".format(vx,vy,vz))
    partB.figMp()

"""
    #Configuración profe
    q=-1.6e-19
    B0=600e-6
    x0=0
    y0=0
    z0=0
    K0=18.6*(1.6e-19)
    m=9e-31
    theta=30
    phi=0
"""
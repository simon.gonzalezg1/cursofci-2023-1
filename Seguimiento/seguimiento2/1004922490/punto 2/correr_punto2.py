import numpy as np
from punto2 import Integral

if __name__ =='__main__':

  
  N=1000
    # Definir parámetros del sistema
  n = 2  # cantidad de moles de gas
  T = 300  # temperatura en K
  V_i = 10  # volumen inicial en L
  V_f = 20  # volumen final en L
  R = 8.31  # constante de los gases ideales en J/(mol K)
  solution=Integral(N,n,T,V_i,V_f,R)
 

  print('Valor del trabajo con el método de Monte Carlo:', -n*R*T*solution.valores_integral()[-1])
  print('Valor del trabajo exacto utilizando scipy.integrate.quad:', -n*R*T*solution.integral_real())
  print("Valor teórico del trabajo: ", -n*R*T*np.log(V_f/V_i))








from montecarlo import MonteCarloIntegrator
import numpy as np
# buen trabajo
#5

if __name__=="__main__":

    N = 10000 #Ingrese aqui el numero de muestras para Montecarlo

    def f(x):
        return x**2 * np.cos(x) #funcion a integrar

    integrator = MonteCarloIntegrator(f, 0, np.pi, N)
    integrator.compare_methods()
    integrator.plot()
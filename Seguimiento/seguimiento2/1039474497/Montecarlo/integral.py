import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad

def integral():
    # Función a integrar
    f = lambda x: x**2 * np.cos(x)
    
    # Intervalo de integración
    a, b = 0, np.pi
    
    # Cálculo de la integral
    I, _ = quad(f, a, b)
    
    # Graficación de la función
    x = np.linspace(a, b, 100)
    y = f(x)
    plt.plot(x, y)
    plt.title("Gráfica de la función x^2 * cos(x)")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.show()
    
    # Impresión del resultado
    print("La integral de la función x^2 * cos(x) en el intervalo [0, pi] es:", I)



    

integral()
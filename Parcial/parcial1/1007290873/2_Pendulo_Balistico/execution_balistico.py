from PenduloBalistico import penduloBalistico, penduloBalisticoGeneral

if __name__ == "__main__":

    # Definimos algunos valores para los parámetros de entrada de la clase, que nos servirán de prueba

    L = 0#0.5
    M = 0.5
    m = 0.2
    alpha = 30

    # Creamos un objeto de la clase penduloBalistico, con estos valores

    pendulo_balistico_dado_alpha = penduloBalistico(L, M, m, alpha)
    print("Velocidad inicial de la bala: {} m/s. Dado un ángulo de {} °".format(pendulo_balistico_dado_alpha.VelocidadBala(), alpha))

    pendulo_balistico_dado_alpha.GraficaOscilacion()

    # Con los mismos valores creamos un objeto de la clase penduloBalisticoGeneral 

    pendulo_balistico_dado_alpha_general = penduloBalisticoGeneral(L, M, m, alpha = alpha)

    # Hacemos uso del método VelocidadMinimaUnaVuelta

    print("Velocidad mínima para dar una vuelta: {} m/s".format(pendulo_balistico_dado_alpha_general.VelocidadMinimaUnaVuelta()))
    
    pendulo_balistico_dado_alpha_general.GraficaOscilacionConEnergía()

    # Creamos un objeto de esta misma clase, pero dando la velocidad inicial

    v0 = 1
    pendulo_balistico_dado_v0 = penduloBalisticoGeneral(L, M, m, v0 = v0)
    print("Ángulo que alcanza el bloque: {} °. Dado una velocidad inicial de {} m/s".format(pendulo_balistico_dado_v0.DesviacionAngulo(), v0))

    
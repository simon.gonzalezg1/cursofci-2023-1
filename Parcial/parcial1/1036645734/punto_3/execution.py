

from EDO import PenduloSimple

if __name__=="__main__":

    def f(theta, omega):
        f = -16*theta
        return f

    #a,b,n,y0,f,point=0,5,9,-3,f,1.9
    sol_euler=PenduloSimple(a=0,b=5,n=50,ang_ini=1,f=f,wi=0, point=False)
    sol_euler.grafica()
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.integrate import odeint
###################################

sns.set()
#Clse para la solución de la ecuación diferencial test
class sol_gen:
    def __init__(self,y0,h,xmax,f):
    #Inicialización de las variables
        self.y0=y0
        self.h=h
        self.xmax=xmax
        self.f=f
        self.x=np.arange(0,xmax,h)
        self.y=np.zeros(len(self.x))
        self.y[0]=y0    
    
    # Método de Euler para la ecuación diferencial test
    def euler_test(self):
        for i in range(len(self.x) - 1):
            self.y[i+1] = self.y[i] + self.f(self.y[i],self.x[i]) * self.h
        return self.y
    
    # Solución analítica de la ecuación diferencial test
    def sol_scipy(self):
        y_sc = odeint(self.f, self.y0, np.linspace(0, self.xmax, 100))
        return y_sc
    
    #Grafica de las soluciones de la ecuación diferencial test
    def graficar(self):
        plt.figure(figsize=(16,9))
        plt.title("Solución numérica y analítica para la ecuación diferencial test",fontsize=20)
        plt.plot(self.x, self.euler_test(),color="red",label="y",lw=4)
        plt.plot(self.x, self.sol_scipy(),color="black",label="Solución analítica")
        plt.xlabel("x",fontsize=20)
        plt.ylabel("y",fontsize=20)
        plt.legend(fontsize=20)
        plt.savefig("Grafica_test.png")
        plt.grid()
        plt.show()


    


# Creación de la clase pendulo simple con el método de Euler
class pendulo_Simple:
    def __init__(self,theta0, omega0, dt, tmax,f1,f2,eq,vector):
#Inicialización de las variables
        self.f1=f1
        self.f2=f2
        self.eq=eq
        self.vector=vector
        self.theta0 = theta0
        self.omega0 = omega0
        self.tmax = tmax
        self.dt=dt
        self.t = np.arange(0, tmax, dt)
        self.theta = np.zeros(len(self.t))
        self.omega = np.zeros(len(self.t))
        self.theta[0] = theta0
        self.omega[0] = omega0



# Método de Euler para el pendulo
    def euler(self):
        for i in range(len(self.t) - 1):
            self.omega[i+1] = self.omega[i] + self.f1(self.theta[i],self.t[i]) * self.dt
            self.theta[i+1] = self.theta[i] + self.f2(self.omega[i+1],self.t[i]) * self.dt
        return self.omega,self.theta
    
    def sol_num(self):
        y_num = odeint(self.eq, self.vector, self.t)
        return y_num
    
    # Gráfica de la solución para theta
    def graficar2(self):
        plt.figure(figsize=(16,9))
        plt.title("Solución numérica y analítica para el pendulo simple",fontsize=20)
        plt.plot(self.t, self.euler()[0],color="red",label=r"$\omega$",lw=4)
        plt.plot(self.t, self.sol_num()[:,1],color="black",label="Solución analítica")
        plt.xlabel("Tiempo",fontsize=20)
        plt.ylabel(r"$\omega$",fontsize=20)
        plt.legend(fontsize=20)
        plt.savefig("Grafica_omega.png")
        plt.grid()
        plt.show()

    # Gráfica de la solución para omega
    def graficar3(self):
        plt.figure(figsize=(16,9))
        plt.title("Solución numérica y analítica para el pendulo simple",fontsize=20)
        plt.plot(self.t, self.euler()[1],color="black",label=r"$\theta$",lw=4)
        plt.plot(self.t, self.sol_num()[:,0],color="red",label="Solución analítica")
        plt.xlabel("Tiempo",fontsize=20)
        plt.ylabel(r"$\theta$",fontsize=20)
        plt.legend(fontsize=20)
        plt.savefig("Grafica_theta.png")
        plt.grid()
        plt.show()






    





        
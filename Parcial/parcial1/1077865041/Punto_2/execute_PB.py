import Pendulo_B as pb
import numpy as np
#Funciones
def f1(theta,t):
    g=9.8
    l = 0.625
    return -g/l*(np.sin(theta))

if __name__ == '__main__':
    print("Veamos las gráficas pedidas:")
    a=pb.Pendulo_B(0,0.5,0.625,0,9.8,0.1,5,f1,0)
    print("La velocidad de las masas juntas justo despues del impacto es:",a.velocidad())
    print("El ángulo desplazado es:",a.angulo())
    b=pb.Velocidad_minima(0.1,0.5,0.625,0.5,9.8,0.1,5,f1,0,91)
    print("La velocidad mínima de la bala para una vuelta completa es:",b.velocidad_min())
    print("La velocidad mínima de la bala unida al bloque para que el bloque haga una vuelta completa es:",b.velocidad_min2())

    #Veamos el oscilador armónico
    c=pb.sol_gen(0.01,50)
    c.graficar()

import numpy as np  # importamos la liberia
import  matplotlib.pyplot as plt # importamos la libreria
class diferenciales ():
    def __init__(self,h,f,xo,yo):
        self.h=h
        self.xo=xo
        self.yo=yo
        self.f=f
        
        
        

    def recurrencia(self):
        contadorx=[]*52
        contadory=[]*52
        suma=0
        m=[0]*52
        m[0]=self.yo
        m.append(self.yo)
        y_1=self.yo+self.h*self.f(self.xo)
        for i in range (0,51):
        
            x_n=self.xo+self.h*i  # x se va actualizando
            contadorx.append(x_n) # guardo los valores de x actualizados
             # valor de i
            yn = m[i]

            yit =yn+self.h*self.f(x_n)
            m[i+1]=yit
            contadory.append(yn)
        return contadory  
    def arreglo(self):
        x=np.arange(0,5.1,0.1)
        return x
    def funcion_analica(self):
        funcion=np.cos(4*self.arreglo())
        return funcion
    def grafica(self):
         plt.plot(self.arreglo(),self.funcion_analica())
    def funcion_numerica(self):
         funcion1=np.cos(4*self.arreglo())     
         return funcion1
    def grafica(self):
        plt.figure(figsize=(10,8))
        plt.plot(self.arreglo(), self.funcion_analica(),color="purple",lw=3,label="analitica")
        plt.plot(self.arreglo(),self.recurrencia(),label="numerica")
        
        
        plt.xlabel(" t")
        plt.ylabel(" thetha")
        plt.grid()
        plt.title("solucion analitica y numerica")
        plt.savefig("pendulos.png")
        
        plt.show()
        
    

     

             
    #def grafica(self):
        #plt.figure(figsize=(10,8))
        #plt.plot(self.arreglo(), self.funcion_analica(),color="purple",lw=3,label="analitica")
        #plt.plot(self.arreglo(),self.recurrencia(),label="numerica")  
            
      
     






class pendulo1():
    def __init__(self,t):
         self.t=t

      




          
          
    def tiempo_arreglo(self):
          tiempo_arreglo=np.arange(0,5.1,0.1)    
          return tiempo_arreglo
    def funcion_pendulo(self):
          
          thetha1=np.cos(4*self.tiempo_arreglo()) 
          return thetha1

    def grafica(self):
          plt.plot(self.tiempo_arreglo(),self.funcion_pendulo())
          plt.grid ()
          plt.savefig("pendulo_simple_analitico.png")   
    def grafica1(self):
          plt.plot(self.tiempo_arreglo(),self.funcion_pendulo(),color="red")
          plt.grid ()
          plt.savefig("pendulo_simple_numerico1.png")      
        

          
      
 
import numpy as np
import matplotlib.pyplot as plt
class pendbalis():
    """
    La clase recibe atributos de masa de la bala, masa del bloque, radio de la cuerda y gravedad.
    A partir de esto los metodos de maxangle y velocity calculan los parametros que les dan su nombre
    a partir de una entrada del dato conocido.

    Los parametros necesarios para definir la clase son:    
    - Masa del proyectil: m (Real positivo en SI)
    - Masa del bloque: M (Real positivo en SI)
    - Longitud de la cuerda: R (Real positivo en SI)
    - Aceleracion gravitacional: g (Real positivo en SI)

    """

    def __init__(self, m, M, R, g):
        self.massbull = m
        self.massblock = M
        self.radius = R
        self.gravity = g

    # Calcula un angulo maximo dada una velocidad inicial

    def maxangle(self, v):
        if v >= 2*(1 + self.massbull/self.massblock)*np.sqrt(self.gravity * self.radius):
            return 180
        theta = np.arccos(1 - ((1 + self.massbull/self.massblock)**(-2) * v**2 / (2*self.radius*self.gravity)))
        return theta * 180/np.pi

    # Calcula una velocidad inicial dado un angulo maximo

    def velocity(self, theta):
        theta = theta * np.pi/180
        if theta < 0 or theta > 180:
            raise Exception("Ingrese un angulo valido")
        v = (1 + self.massbull/self.massblock) * np.sqrt(2* self.gravity * self.radius*(1-np.cos(theta)))
        return v
    
    # Metodo que grafica el movimiento del bloque en el eje y despues de la colision dado un theta maximo

    def graph(self, theta):
        theta = theta * np.pi/180
        if theta < 0 or theta > 90:
            raise Exception("Ingrese un angulo valido")
        w = np.sqrt(self.gravity/self.radius)
        t = np.arange(0,5,0.01)
        y = self.radius* (1 - np.cos(theta*np.sin(w*t)))
        
        plt.figure(figsize=(10,8))
        plt.plot(t, y , label="Trayectoria",color="black")
        plt.grid()
        plt.xlabel("Tiempo (segundos)")
        plt.ylabel("Altura (metros)")
        plt.legend()
        plt.savefig("figura_oscarmpend.png")
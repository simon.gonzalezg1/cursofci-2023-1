import numpy as np
import matplotlib.pyplot as plt 
class movparabolico():
    """
    Clase para el movimiento parabolico junto los metodos para calcular diferentes cantidades, arreglos
    y para realizar graficas. 

    Los parametros necesarios para definir la clase son:
    - Posicion inicial en x: x0 (Numero real en SI)
    - Posicion inicial en y: y0 (Numero real en SI)
    - Magnitud de la velocidad inicial: v0 (Numero real en SI)
    - Angulo que forma la velocidad con el eje x positivo: alpha (Grados)

    """
    def __init__(self, x0=0., y0=0., v0=0., alpha=0.):
        self.x0 = x0
        self.y0 = y0
        self.v0 = v0
        self.alpha = alpha
    
    # A continuacion se definen los metodos:

    # Velocidad inicial en x:
    def v0x(self):
        return self.v0*np.round(np.cos(self.alpha * np.pi/180),3)

    # Velocidad inicial en y:
    def v0y(self):
        return self.v0*np.round(np.sin(self.alpha * np.pi/180),3)

    # Tiempo de vuelo:
    def flightime(self,a):
        try:
            v0y = self.v0y()
            tmax = np.array([(- v0y + np.sqrt(v0y**2 - 2*a*self.y0))/(a), 
                            (- v0y - np.sqrt(v0y**2 - 2*a*self.y0))/(a)])
            tmax = np.max(tmax)
            return tmax
        except:
            return "Error en calculo de tmax, revisar parámetros" 
    
    # Posicion en x dado un tiempo:
    def x(self,t):
        v0x = self.v0x()
        return self.x0 + t*v0x
    
    # Posicion en x dado un tiempo y una aceleracion en x:
    def xac(self, t, ax):
        return self.x0 + self.v0x() * t + ax * t**2 * 1/2

    # x final dado una aceleracion en y:
    def xmax(self,a):
        tf = self.flightime(a)
        return self.x(tf)
    
    # x final dado una aceleracion en x y y:
    def xmaxac(self, ax, ay):
        tf = self.flightime(ay)
        arrx = self.arrxac(ax, ay)
        return np.max(arrx)
    
    # Posicion en y dado un tiempo y una aceleracion en y:
    def y(self,t,a):
        v0y = self.v0y()
        y = self.y0 + v0y*t + a* t**2 * 1/2
        if y < 0: return float(0)
        return y

    # Altura maxima
    def ymax(self,a):
        tymax = - self.v0y()/a 
        return self.y(tymax, a)
    
    # Arreglo de tiempos del movimiento dada una aceleracion en y:
    def arrtime(self,a):
        t = np.linspace(0, self.flightime(a), 1000)
        return t 
    
    # Arreglo de posiciones en x dada una aceleracion en y:
    def arrx(self, a):
        t = self.arrtime(a)
        x = np.array([])
        for i in t:
            x = np.append(x, self.x(i))
        return x

    # Posiciones en x durante la trayectoria dadas aceleraciones en x y en y:
    def arrxac(self, ax, ay):
        t = self.arrtime(ay)
        x = np.array([])
        for i in t:
            x = np.append(x, self.xac(i, ax))
        return x
    
    # Arreglo de posiciones en y dada una aceleracion en y:
    def arry(self, a):
        t = self.arrtime(a)
        y = np.array([])
        for i in t:
            y = np.append(y, self.y(i,a))
        return y

    # Metodo que grafica la trayectoria y la guarda en un archivo .png:
    def graph(self,a):
        
        tfin = self.flightime(a)
        x = self.arrx(a)
        y = self.arry(a)
        
        plt.figure(figsize=(10,8))
        plt.plot(x, y , label="Trayectoria",color="black")
        plt.scatter(self.x0, self.y0, label="Punto inicial",color="blue")
        plt.scatter(self.x(tfin), self.y(tfin,a), label="Punto final",color="red")
        plt.grid()
        plt.xlabel("Eje x")
        plt.ylabel("Eje y")
        plt.legend()
        plt.savefig("MovParabolico.png")
    
    # Metodo que grafica la trayectoria ahora con aceleracion en x y en y:
    def graphxac(self, ax, ay):
        
        tfin = self.flightime(ay)
        x = self.arrxac(ax, ay)
        y = self.arry(ay)
        
        plt.figure(figsize=(10,8))
        plt.plot(x, y , label="Trayectoria",color="black")
        plt.scatter(self.x0, self.y0, label="Punto inicial",color="blue")
        plt.scatter(self.xac(tfin, ax), self.y(tfin,ay), label="Punto final",color="red")
        plt.grid()
        plt.xlabel("Eje x")
        plt.ylabel("Eje y")
        plt.legend()
        plt.savefig("MovParabolicoXac.png")

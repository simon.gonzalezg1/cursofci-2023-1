from tiroparabolico import tiroParabolico
from tiroparabolico import Objeto


if __name__=="__main__":

    #Configuración
    velinit=110             # velocidad inicial del objeto en m/s
    alpha=90                # ángulo de lanzanmiento respecto a la horizontal en grados
    g=9.8                   # gravedad en m/s**2
    h0=50                   # altura inicial
    x0=0                    # posición inicial en x
    ax=5                # aceleración debido a la resistencia del viento en m/s**2

    #Llamar clase
    tirop=tiroParabolico(velinit,alpha,g,h0,x0,ax)

    #Llamar métodos
    #tirop.alcanceMax()
    #tirop.alturaMax()
    tirop.figMp()


    #Clase heredada
    objeto=Objeto(velinit,alpha,g,h0,x0,ax)
    objeto.Lanzamiento()
    objeto.tMaxVuelo()
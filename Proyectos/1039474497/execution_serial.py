
from serialConnection import ArduinoConnection


if __name__=="__main__":
    print("ejecutando conexion serial")

    arduino = ArduinoConnection('COM9', baudrate=115200)

    if arduino.connect():
        print('Conexión establecida correctamente')
    else:
        print('No se pudo establecer la conexión')

    arduino.send('Hello World')
    respuesta = arduino.read()
    print(respuesta)

    #arduino.disconnect()


